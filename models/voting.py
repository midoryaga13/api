from typing import List
from pydantic import BaseModel

class Score(BaseModel):
    criteria_id: int
    avarage: float

class ScoringModel(BaseModel):
    candidate_uid: str
    match_uid: str
    round_id: int
    voting_scores: List[Score]
