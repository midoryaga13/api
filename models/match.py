from datetime import datetime
from enum import auto
from typing import List
from pydantic import BaseModel
from fastapi_utils.enums import StrEnum
class MatchStatus(StrEnum):
    REGISTERED = auto()
    PENDING = auto()
    FINISHED= auto()
class CreateMatch(BaseModel):
    participant_uids: List[str]
    competition_step_uid: str
    start_date: datetime
    duration: int
    round_number: int

class UpdateMatch(CreateMatch):
    uid: str
    status: MatchStatus

class ActivateMatch(BaseModel):
    match_id: str
    jury_uids: List[str]
