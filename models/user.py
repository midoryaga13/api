from typing import Optional
from fastapi import File
from pydantic import BaseModel, EmailStr


class User(BaseModel):
    public_id : str
    firstname: str
    lastname: str
    email: str
    phonenumber: str
    role_id: int
    gender_id: int
    password: str

class RegisterUser(BaseModel):
    firstname: str
    lastname: str
    email: EmailStr
    phonenumber: Optional[str]
    role_id: int
    gender_id: int
    password: str


class Role(BaseModel):
    id: int
    title_fr: str
    title_en: str

class UpdateUser(BaseModel):
    firstname: Optional[str]
    lastname: Optional[str]
    phonenumber: Optional[str]

class UserLogin(BaseModel):
    email: EmailStr
    password: str

class RegisterSimpleUser(BaseModel):
    firstname: str
    lastname: str
    email: EmailStr
    phonenumber: Optional[str]
    gender_id: int
    password: str
    # file: bytes = File(...),

