from datetime import datetime
from typing import List, Optional
from unicodedata import category
from pydantic import BaseModel
from enum import auto
from fastapi_utils.enums import StrEnum

from models.user import RegisterUser
class CategoryType(StrEnum):
    AGE = auto()
    SEX = auto()

class NotationType(StrEnum):
    SCORE = auto()
    VOTING = auto()

class CompetitionStatus(StrEnum):
    DEACTIVATED = auto()
    PENDING = auto()
    FINISHED = auto()
    CANCELED = auto()

class CreateCategory(BaseModel):
    type: CategoryType
    name: str
    rules: str
    start_date: datetime
    end_date: datetime
    competition_format_id: int
    max_number_of_participants: int
    notation_type: NotationType

class UpdateCategory(BaseModel):
    uid: str
    type: Optional[CategoryType]
    name: Optional[str]
    rules: Optional[str]
    start_date: Optional[str]
    end_date: Optional[str]
    competition_format_id: Optional[int]
    max_number_of_participants: Optional[int]
    notation_type: Optional[NotationType]

class DeleCategory(BaseModel):
    competition_uid: str
    category_uid: str

class CreateCompetition(BaseModel):
    name: str
    start_date: datetime
    end_date: datetime
    location: str
    sport_id: int
    detail_about: str
    rules: str
    registration_deadline: datetime
    competition_format_id: int
    max_number_of_participants: int
    categorie: Optional[List[CreateCategory]]

class UpdateCompetition(BaseModel):
    uid: str
    name: Optional[str]
    start_date: Optional[str]
    end_date: Optional[str]
    event_type: Optional[int]
    sport_id: Optional[int]
    detail_about: Optional[str]
    rules: Optional[str]
    registration_deadline: Optional[str]
    competition_format_id: Optional[int]
    max_number_of_participants: Optional[int]

class Competition(UpdateCompetition):
    status: str
    is_deleted: bool
    date_added: datetime
    date_modified: datetime

class CreateCompetitionStep(BaseModel):
    competition_uid: str
    start_date: datetime
    end_date: datetime
    competition_format_id: int
    max_number_of_participants: int
    category_uid: Optional[str]
    auto_generate_match: bool
class CompetitionStep(CreateCompetitionStep):
    status : CompetitionStatus
    is_deleted: bool
    status: str
    date_added: datetime
    date_modified: datetime

class CompetitionRegistration(BaseModel):
    candidate_uid: Optional[List[str]]
    competition_uid: str
    step_uid: str
    candidate: Optional[List[RegisterUser]]
class BasicTranslate(BaseModel):
    id: int
    title_fr: str
    title_er: str
