import datetime
import time
from fastapi_utils.enums import StrEnum
from enum import auto

from pydantic import BaseModel, Json

class NotificationType(StrEnum):

    BASIC_MESSAGE = auto()
    EVENT_NEW_NOTIFICATION = auto()
    NEW_PUSH_NOTIFICATION = auto()

class Notification(BaseModel):
    id: int
    n_type: NotificationType
    user_id: str
    payload_json: Json
    read: bool
    timestamp: time
    date_added: datetime
    date_modified: datetime