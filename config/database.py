from pymongo import MongoClient
from core.config import Config
import gridfs
# from pymongo.collection import ReturnDocument

# client = MongoClient("mongodb+srv://georges:7UKj7GBwXPjfTxv@cluster0.xnqwn.mongodb.net/test?retryWrites=true&w=majority", 
#                     document_class=RawBSONDocument, uuidRepresentation='standard')
# db = client.test
conn = MongoClient(Config.MONGO_DATABASE_URL, UuidRepresentation= "standard")
# conn = MongoClient("mongodb://localhost:27017/test", UuidRepresentation= "standard", document_class= ReturnDocument)
db = conn.local
dbf = conn.grid_file
fs = gridfs.GridFS(db)
