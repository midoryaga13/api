# 1 - uninstall old docker version
sudo apt-get remove docker docker-engine docker.io containerd runc

# 2- setup the docker repositoy

# 2-1 Update the apt package index and install packages to allow apt to use a repository over HTTPS
sudo apt-get update

sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# 2-2 Add Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# 2-3 set up the stable repository

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


# 3- install docker-engine
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

# 4- verify the docker-engine installation

sudo service docker start

sudo docker run hello-world

