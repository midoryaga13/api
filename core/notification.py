import redis
import json

from core.config import Config


class NotificationPublisher():
  
  def __init__(self, host: str, port: int, db: int):
    self.redis = redis.Redis(host=host, port=port, db=db)

  def publish(self, channel: str, type: str, data: dict):
    try:
      self.redis.publish('message', json.dumps({'channel': channel, 'type': type, 'data': data}))
    except Exception as e:
      print(str(e))

notificationPublisher = NotificationPublisher(host=Config.REDIS_HOST, port=Config.REDIS_PORT, db=Config.REDIS_SOCKET_DB)