from fastapi import HTTPException
import jwt
from pydantic import ValidationError
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi import Request, HTTPException

from schemas.user import TokenPayload
from core import security
from core.config import Config
from core.i18n import t
from crud.base import CRUDBase

def has_role(user, role: str):
    if role.lower() == "administrator":
        return user["role_id"] == 1
    if role.lower() == "sub-administrator":
        return user["role_id"] == 2
    if role.lower() == "auditor":
        return user["role_id"] == 3
    if role.lower() == "judge":
        return user["role_id"] == 4
    if role.lower() == "user":
        return user["role_id"] == 5

    return False

class TokenRequired(HTTPBearer):

    def __init__(self, auto_error: bool = True, roles: list = [],):
        self.roles = roles
        super(TokenRequired, self).__init__(auto_error=auto_error)

    async def __call__(self, request: Request):
        required_roles = self.roles
        credentials: HTTPAuthorizationCredentials = await super(TokenRequired, self).__call__(request)
        
        if credentials:
            if not credentials.scheme == "Bearer":
                raise HTTPException(status_code=403, detail=t("invalid-session"))
            
            token_data = self.verify_jwt(credentials.credentials)
            if not token_data:
                raise HTTPException(status_code=403, detail=t("expired-or-invalid-session"))
            
            current_user = CRUDBase.get_by_public_id(id=token_data.sub)
            if not current_user:
                raise HTTPException(status_code=403, detail=t("expired-or-invalid-session"))
            
            if required_roles:
                if not self.verify_role(roles=required_roles, user=current_user):
                    raise HTTPException(status_code=403, detail=t("not-authorized"))

            return current_user
        else:
            raise HTTPException(status_code=403, detail=t("invalid-code"))

    
    def verify_jwt(self, token: str) -> bool:

        isTokenValid: bool = False

        try:
            payload = jwt.decode(
                token, Config.SECRET_KEY, algorithms=[security.ALGORITHM]
            )
            token_data = TokenPayload(**payload)
            return token_data
        except (jwt.InvalidTokenError, ValidationError) as e:
            print(e)
            payload = None
            
        if payload:
            isTokenValid = True
        return isTokenValid

    
    def verify_role(self, roles, user) -> bool:
        has_a_required_role = False
        if user["role_id"]:
            if isinstance(roles, str):
                user_has_role = has_role(user, roles[0])
                if user_has_role:
                    has_a_required_role = True
            else:
                for role in roles:
                    user_has_role = has_role(user, role)
                    if user_has_role:
                        has_a_required_role = True
                        break
        return has_a_required_role
