# from sqlalchemy.sql.elements import or_
# from app.main import models
# import logging
# from datetime import datetime, timedelta
# from pathlib import Path
# from typing import Any, Dict, Optional
# from app.main.worker import celery
# from app.main.models.db.session import SessionLocal
# import emails
# from emails.template import JinjaTemplate
# import jwt
# from app.main.core.config import Config
# from app.main.worker import celery
# import io
# from dateutil.relativedelta import relativedelta
# import pytz
# from app.main.util import helper


# @celery.task(name="send_email.task")
# def send_email(
#     email_to: str,
#     subject_template: str = "",
#     html_template: str = "",
#     environment: Dict[str, Any] = {},
# ) -> None:
#     assert Config.EMAILS_ENABLED, "aucune configuration fournie pour les variables de messagerie"
#     message = emails.Message(
#         subject=JinjaTemplate(subject_template),
#         html=JinjaTemplate(html_template),
#         mail_from=(Config.EMAILS_FROM_NAME, Config.EMAILS_FROM_EMAIL),
#     )
#     smtp_options = {"host": Config.SMTP_HOST, "port": Config.SMTP_PORT}
#     if Config.SMTP_TLS:
#         smtp_options["tls"] = False
#     if Config.SMTP_USER:
#         smtp_options["user"] = Config.SMTP_USER
#     if Config.SMTP_PASSWORD:
#         smtp_options["password"] = Config.SMTP_PASSWORD
#     response = message.send(to=email_to, render=environment, smtp=smtp_options)
#     logging.info(f"résultat de l'email envoyé: {response}")


# def send_test_email(email_to: str) -> None:
#     project_name = Config.PROJECT_NAME
#     subject = f"{project_name} - Test email"
#     with io.open(Path(Config.EMAIL_TEMPLATES_DIR) / "test_email.html", encoding="utf-8") as f:
#         template_str = f.read()
#     task = send_email.delay(
#         email_to=email_to,
#         subject_template=subject,
#         html_template=template_str,
#         environment={"project_name": Config.PROJECT_NAME, "email": email_to},
#     )
#     logging.info(f"new send mail task with id {task.id}")


# def send_reset_password_email(email_to: str, email: str, token: str, name: str, hour: str) -> None:
#     # project_name = Config.PROJECT_NAME
#     subject = "YAFOY - Récupération du mot de passe"
#     with io.open(Path(Config.EMAIL_TEMPLATES_DIR) / "reset_password.html", encoding="utf-8") as f:
#         template_str = f.read()
#     # server_host = Config.SERVER_HOST
#     # link = f"{server_host}/reset-password?token={token}"
#     task = send_email(
#         email_to=email_to,
#         subject_template=subject,
#         html_template=template_str,
#         environment={
#             "code": token,
#             "name": name,
#             "email": email_to,
#             "valid_hours": hour
#         },
#     )
#     # logging.info(f"new send mail task with id {task.id}")


# def send_new_account_email(email_to: str, username: str, password: str) -> None:
#     project_name = Config.PROJECT_NAME
#     subject = f"YAFOY - Nouveau compte pour l'utilisateur {username}"
#     with io.open(Path(Config.EMAIL_TEMPLATES_DIR) / "new_account.html", encoding="utf-8") as f:
#         template_str = f.read()
#     link = Config.SERVER_HOST
#     task = send_email.delay(
#         email_to=email_to,
#         subject_template=subject,
#         html_template=template_str,
#         environment={
#             "project_name": Config.PROJECT_NAME,
#             "username": username,
#             "password": password,
#             "email": email_to,
#             "link": link,
#         },
#     )
#     logging.info(f"new send mail task with id {task.id}")


# def confirmation_create_account_mail(email: str, code: str, name: str) -> None:
#     project_name = Config.PROJECT_NAME
#     subject = f"YAFOY - Nouveau compte pour l'utilisateur {name}"
#     with io.open(Path(Config.EMAIL_TEMPLATES_DIR) / "new_account.html", encoding="utf-8") as f:
#         template_str = f.read()
#     link = Config.SERVER_HOST
#     task = send_email.delay(
#         email_to=email,
#         subject_template=subject,
#         html_template=template_str,
#         environment={
#             "project_name": Config.PROJECT_NAME,
#             "username": name,
#             "code": code,
#             "email": email,
#             "link": link,
#         },
#     )
#     logging.info(f"new send mail task with id {task.id}")



# def register_a_pre_user_account_mail(email: str, country: str, city: str, name: str) -> None:
    
#     subject = "Yafoy App - Nouvelle Préinscription"
#     with open(Path(Config.EMAIL_TEMPLATES_DIR) / "new_account_pre_register.html") as f:
#         template_str = f.read()
#     dr_email = Config.DR_EMAIL
#     task = send_email.delay(
#         email_to=dr_email,
#         subject_template=subject,
#         html_template=template_str,
#         environment={
#             "country": country,
#             "name": name,
#             "city": city,
#             "email": email
#         },
#     )
#     logging.info(f"new send mail task with id {task.id}")


# @celery.task(name="automatic_reminder_sent_by_email_to_the_user_unactived_account")
# def automatic_reminder_sent_by_email_to_the_user_unactived_account():

#     db = SessionLocal()

#     utc = pytz.UTC
#     current_date = datetime.now(utc)
#     print(current_date)

#     users = db.query(models.User)\
#             .filter(or_(models.User.status == models.UserStatusType.UNVALIDATED, models.User.status == models.UserStatusType.UNACTIVED))

#     for user in users.all():
#         diff = relativedelta(current_date, user.date_added)
#         days = diff.days
#         print("--------USER-----------")
#         print("days")
#         print(days)
#         print("---------------------")

#         if user.email:
#             print("User email")
#             print(user.email)
#             print("---------------------")

#         if user.phonenumber:
#             print("User phonenumber")
#             print(user.phonenumber)
#             print("---------------------")

#         if days == 14:

#             # Get a code generate
#             code = helper.generate_code(length=6, end=True)

#             # Register the user code
#             user_code = models.UserPreRegister(
#                 code=str(code),
#                 user_id=user.public_id,
#                 expired_date=datetime.now() + timedelta(hours=3)
#             )
#             db.add(user_code)
#             db.commit()

#             # Send mail to user to active her account
#             confirmation_create_account_mail(email=user.email, name=(user.firstname or "")+" "+(user.lastname or ""), code=str(code))
            
#         elif days > 14:
#             # Drop the user account
#             db.delete(user)
#             db.commit()
#         else:
#             pass

#     return "success"

# # print("yerererererer")
# # automatic_reminder_sent_by_email_to_the_user_unactived_account()
# # print("yyyyyyeeeeeeeeeeeeeeeeeee")