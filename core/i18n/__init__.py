from fastapi import Request
from contextvars import ContextVar
import logging

from core.config import Config
from .languages import langs

language = ContextVar("lang", default=Config.PREFERED_LANGUAGE)

########## add middleware for language processing ###########
async def add_process_language_header(request: Request, call_next):
    accepted_languages = langs.keys()
    lang = Config.PREFERED_LANGUAGE
    if "lang" in request.headers and request.headers["lang"]:
        if  request.headers["lang"] in accepted_languages:
            lang = request.headers["lang"]
    elif "Accept-Language" in request.headers and request.headers["Accept-Language"]:
        user_browser_languages = request.headers["Accept-Language"].split(",")
        for user_browser_language in user_browser_languages:
            user_browser_language = user_browser_language.split(";")[0].split("-")[0].lower()
            if user_browser_language in accepted_languages:
                lang = user_browser_language
                break

    language.set(lang)
    response = await call_next(request)
    return response


def get_language():
    try:
        return language.get()
    except Exception as e:
        logging.info(f"Failed get user language for language ContextVar: {e}")
        return Config.PREFERED_LANGUAGE



def t(key: str, language: str=None):
    try:
        lang = language or str(get_language())
    except Exception as e:
        logging.info(f"Failed get user language: {e}")
        lang = Config.PREFERED_LANGUAGE
        
    try: 
        return langs[lang][key] if key in langs[lang] else  langs[Config.PREFERED_LANGUAGE][key]
    except Exception as e:
        logging.info(f"Failed translate message: {e}")
        return key