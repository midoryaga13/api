import os
from pydantic import BaseSettings, AnyHttpUrl, EmailStr, validator
from typing import Optional, Dict, Any, List, Union

def get_secret(secret_name, default):
    try:
        with open('/run/secrets/{0}'.format(secret_name), 'r') as secret_file:
            return secret_file.read().strip()
    except IOError:
        return os.getenv(secret_name, default)

class ConfigClass(BaseSettings):

    SECRET_KEY: str = get_secret("SECRET_KEY", 'esm_secret_key')
    ALGORITHM: str = get_secret("ALGORITHM", 'HS256')

    ADMIN_KEY: str = get_secret("ADMIN_KEY", "Esm@Key!2k22")
    ACCESS_ADMIN_KEY: str = get_secret("ADMIN_KEY", "Esm@Key@!@Secret@2K22")


    # 60 minutes * 24 hours * 365 days = 365 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = int(get_secret("ACCESS_TOKEN_EXPIRE_MINUTES", 60 * 24 * 1))


    MONGO_DATABASE_URL: str = get_secret("MONGO_DATABASE_URL", 'mongodb://root_e_sport_manager:e1c8c812_3553_4e33_a4a6_423a74848ee0@esm_database/test?authSource=admin&retryWrites=true&w=majority')
    PREFERED_LANGUAGE: str = get_secret("PREFERED_LANGUAGE", 'fr')

    API_V1_STR: str = get_secret("EMAILS_FROM_NAME", "/api/v1")
    PROJECT_NAME: str = get_secret("PROJECT_NAME", "ESM API")
    PROJECT_VERSION: str = get_secret("PROJECT_VERSION", "0.1")


    # Redis config
    
    REDIS_HOST: str = get_secret("REDIS_HOST", "esm_redis")
    REDIS_PORT: int = get_secret("REDIS_PORT", 6379)
    REDIS_DB: int = get_secret("REDIS_DB", 2)
    REDIS_SOCKET_DB: int = get_secret("REDIS_SOCKET_DB", 3)
    REDIS_NOTIFICATION_DB: int = get_secret("REDIS_NOTIFICATION_DB", 3)
    REDIS_CHARSET: str = get_secret("REDIS_CHARSET", "UTF-8")
    REDIS_DECODE_RESPONSES: bool = get_secret("REDIS_DECODE_RESPONSES", True)

    CELERY_RESULT_BACKEND = "redis://" + REDIS_HOST + ":" + str(REDIS_PORT) + "/" + str(REDIS_DB)
    CELERY_BROKER_URL = "redis://" + REDIS_HOST + ":" + str(REDIS_PORT) + "/" + str(REDIS_DB)  
    CELERY_REDIS_HOST = REDIS_HOST
    CELERY_REDIS_PORT = REDIS_PORT
    CELERY_REDIS_DB = REDIS_DB

    # Google map api key
    GOOGLE_MAP_API_KEY: str = get_secret('GOOGLE_MAP_API_KEY', "AIzaSyD-evsf9rdnPemYJjgysd1fShpBPk8v_vI") # Todo change this after


    # About auth with facebook logging Todo change after
    # FACEBOOK_APP_SECRET_KEY: str = get_secret("FACEBOOK_APP_SECRET_KEY", "af4ec3b1ddc352889d4d8800313f0b7a")
    # FACEBOOK_APP_ID: str = get_secret("FACEBOOK_APP_ID", "691967901575494")

    # About google login Todo change after
    # GOOGLE_CLIENT_ID: str = get_secret("GOOGLE_CLIENT_ID", "536576471894-nc63elrmrdq97eag18m70e270tj1cefh.apps.googleusercontent.com")
    # GOOGLE_CLIENT_SECRET: str = get_secret("GOOGLE_CLIENT_SECRET", "q0EknnSn-EnXrXTKIcG3HM4v")
    # GOOGLE_LOGIN_USER_INFO: str = get_secret("GOOGLE_LOGIN_USER_INFO", 'https://www.googleapis.com/oauth2/v2/userinfo')


    # Config for sms
    # SMS_SERVICE_PLAN_ID = get_secret('SMS_SERVICE_PLAN_ID', 'e3481406a614483c86f0373b86f86842')
    # SMS_TOKEN = get_secret('SMS_TOKEN', '960839d93475490c9cddbb2661d4f0e8')

    # SMTP_TLS: bool =  get_secret("SMTP_TLS", False)
    # SMTP_SSL: bool =  get_secret("SMTP_SSL", False)
    # SMTP_PORT: Optional[int] =  int(get_secret("SMTP_PORT", 25))
    # SMTP_HOST: Optional[str] =  get_secret("SMTP_HOST", "mail.kevmax.com")
    # SMTP_USER: Optional[str] =  get_secret("SMTP_USER", "freelance@kevmax.com")
    # SMTP_PASSWORD: Optional[str] =  get_secret("SMTP_PASSWORD", "pLq44s@f43")
    # EMAILS_FROM_EMAIL: Optional[EmailStr] =  get_secret("EMAILS_FROM_EMAIL", "Kevmax Freelancer <freelance@kevmax.com>")
    # EMAILS_FROM_NAME: Optional[str] =  get_secret("EMAILS_FROM_NAME", "ESM APP")


    @validator("PROJECT_NAME")
    def get_project_name(cls, v: Optional[str], values: Dict[str, Any]) -> str:
        if not v:
            return values["PROJECT_NAME"]
        return v

    EMAIL_RESET_TOKEN_EXPIRE_HOURS: int = int(get_secret("EMAIL_RESET_TOKEN_EXPIRE_HOURS", 48))
    EMAILS_ENABLED: bool = get_secret("EMAILS_ENABLED", True) in ["True", True]
    EMAIL_TEMPLATES_DIR: str = "{}/app/main/templates/emails/render".format(os.getcwd())

    @validator("EMAILS_ENABLED", pre=True)
    def get_emails_enabled(cls, v: bool, values: Dict[str, Any]) -> bool:
        return bool(
            values.get("SMTP_HOST")
            and values.get("SMTP_PORT")
            and values.get("EMAILS_FROM_EMAIL")
        )

    EMAIL_TEST_USER: EmailStr = get_secret("EMAIL_TEST_USER","support@kevmax.com")
    FIRST_SUPERUSER: EmailStr = get_secret("FIRST_SUPERUSER","kevin.wamba@kevmax.com")
    FIRST_SUPERUSER_PASSWORD: str = get_secret("FIRST_SUPERUSER_PASSWORD","test")
    FIRST_SUPERUSER_FIRST_NAME: str = get_secret("FIRST_SUPERUSER_FIRST_NAME","Kevin")
    FIRST_SUPERUSER_LASTNAME: str = get_secret("FIRST_SUPERUSER_LASTNAME","WAMBA")
    USERS_OPEN_REGISTRATION: bool = get_secret("USERS_OPEN_REGISTRATION", False) in ["True", True]


    CELERY_BROKER_URL: str = get_secret("CELERY_BROKER_URL", "redis://redis:6379/0")
    CELERY_RESULT_BACKEND: str = get_secret("CELERY_RESULT_BACKEND", "redis://redis:6379/0")


    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = [
        "https://www.base.kevmax.com"
    ]

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    class Config:
        case_sensitive = True
 
Config = ConfigClass()
