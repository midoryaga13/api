from typing import Generic
from config.database import db
from crud.base import CreateSchemaType, UpdateSchemaType
from schemas.notification import NotificationEntity


class CRUDNotification(Generic[CreateSchemaType, UpdateSchemaType]):
    
    @classmethod
    def unread_notification_count(cls, user_id):
        return db.notification.count_documents({
        "$and": [
            {"$or": 
                [
                    {"read": False}, 
                    {"read": None}
                ]
            },
            {
                "user_id": user_id
            }
            ]
        })
    
    @classmethod
    def mark_notification_as_read(cls, user_id, notification_id=None):
        if notification_id is None:
            db.notification.find_one_and_update(
                {"user_id": user_id},
                {"$set": {'read': True}}
                )
        else:
            db.notification.find_one_and_update(
                {"$and": [{"user_id": user_id}, {"id": notification_id}]},
                {"$set": {'read': True}}
                )
    
    @classmethod
    def get_all_unread_notification(cls, user_id) -> list:
        notif_sch = []
        exist_notif = db.notification.find(
            {"$and": [
                {"user_id": user_id},
                {"$or": 
                [
                    {"read": False}, 
                    {"read": None}
                ]
            }
            ]}
        ).sort({
            "id": -1
        })
        for notif in exist_notif:
            notif_sch.append(NotificationEntity(notif))
        return  notif_sch
    
    @classmethod
    def get_all_notification(cls, user_id):
        notif_sch = []
        exist_notif = db.notification.find(
            {"user_id": user_id}
        ).sort({
            "id": -1
        })
        for notif in exist_notif:
            notif_sch.append(NotificationEntity(notif))
        return  notif_sch