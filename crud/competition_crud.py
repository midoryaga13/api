from datetime import datetime
from typing import Generic
import uuid
import pytz

from fastapi import HTTPException
from config.database import db
from core.i18n import t
from core.security import get_password_hash
from crud.base import CRUDBase, CreateSchemaType, UpdateSchemaType
from models.competition import  CompetitionRegistration, CompetitionStatus, CreateCompetition, CreateCompetitionStep, UpdateCompetition

from schemas.competition import CategoryEntity, StepEntity, competitionEntity
from schemas.user import userEntity
from utils.helper import is_valid_phonenumber, validate_date, validate_email

def validate_competition_format(id: int):
    if id in [1, 2, 3, 4, 5, 6] == False: 
            raise HTTPException(
            status_code=404,
            detail= t("competition-format-not-exist")
        )

utc=pytz.UTC
class CRUDCompetition(Generic[CreateSchemaType, UpdateSchemaType]):
    
    @classmethod
    def get_list_competitions(cls):
        results = []
        cats_sch = []
        steps_sch = []
        datas = db.competition.find({"is_deleted": False})
        for data in datas:
            cats = db.category.find({"$and": [{"competition_uid": data["uid"]}, {"is_deleted": False}]})
            steps = db.competition_step.find({"$and": [{"competition_uid": data["uid"]}, {"is_deleted": False}]})
            if cats:
                for cat in cats:
                    cats_sch.append(CategoryEntity(cat))
            if steps:
                for step in steps:
                    steps_sch.append(StepEntity(step))
            results.append({"competition": competitionEntity(data), "categories": cats_sch, "steps": steps_sch})
            cats_sch = []
            steps_sch = []
        return results
       
    @classmethod
    def delete_a_competition(cls, uid: str):
        try:
            db.competition.find_one_and_update(
                {"$and": [
                    {"uid": str(uid)}, 
                    {"is_deleted": False}
                    ]},
                {"$set": {"is_deleted": True}}
            )
        except Exception as e:
                print(str(e))
                raise HTTPException(
                status_code=500,
                detail= t("server-error")
            )
    
    @classmethod
    def update_a_competition(cls, competition: UpdateCompetition):
        obj_in = {}
        if competition.sport_id and competition.sport_id in [1, 2, 3, 4, 5, 6, 7] == -1: 
            raise HTTPException(
            status_code=404,
            detail= t("sport-not-exist")
        )
        
        if competition.competition_format_id: 
            validate_competition_format(competition.competition_format_id)

        if competition.max_number_of_participants and int(competition.max_number_of_participants) in [0, 1]:
            raise HTTPException(
            status_code=403,
            detail= t("invalid-max-number-of-participants")
        )
        if competition.start_date : validate_date(date_text=competition.start_date)
        if competition.end_date : validate_date(date_text=competition.end_date)
        if competition.registration_deadline : validate_date(date_text=competition.registration_deadline)
        
        if competition.start_date and competition.end_date :
            if (validate_date(date_text=competition.start_date) >= validate_date(date_text=competition.end_date)):
                raise HTTPException(
                status_code=422,
                detail= t("start-date-greater-end-date")
            )
        if competition.registration_deadline and competition.start_date:
            if (validate_date(date_text=competition.registration_deadline) >= validate_date(date_text=competition.start_date)):
                raise HTTPException(
                status_code=422,
                detail= t("registration-deadline-greater-start-date")
            )
        
        if competition.competition_format_id:
            obj_in["competition_format_id"] = competition.competition_format_id
        if competition.name:
            obj_in["name"] = competition.name
        if competition.start_date:
            obj_in["start_date"] = validate_date(date_text=competition.start_date)
        if competition.end_date:
            obj_in["end_date"] = validate_date(date_text=competition.end_date)
        if competition.event_type:
            obj_in["event_type"] = competition.event_type
        if competition.sport_id:
            obj_in["sport_id"] = competition.sport_id
        if competition.detail_about:
            obj_in["detail_about"] = competition.detail_about
        if competition.rules:
            obj_in["rules"] = competition.rules
        if competition.registration_deadline:
            obj_in["registration_deadline"] = validate_date(date_text=competition.registration_deadline)
        if competition.competition_format_id:
            obj_in["competition_format_id"] = competition.competition_format_id
        if competition.max_number_of_participants:
            obj_in["max_number_of_participants"] = competition.max_number_of_participants
        obj_in["date_modified"] = datetime.now()
        if competition.uid:
            try:
                db.competition.update_one(
                    {"uid": competition.uid},
                    update= {"$set": obj_in},
                    upsert=False,
                )
            except Exception as e:
                raise HTTPException(
            status_code=500,
            detail= f'{t("server-error")} : {e}'
        )
        else:
            raise HTTPException(
            status_code=422,
            detail= f'{t("invalid-data")} : {t("pass-competition-uid")}'
        )
    
    @classmethod
    def create_a_competition(cls, competition: CreateCompetition):
        if competition.sport_id in [1, 2, 3, 4, 5, 6, 7] == -1: 
            raise HTTPException(
            status_code=404,
            detail= t("sport-not-exist")
        )
        validate_competition_format(competition.competition_format_id)

        if int(competition.max_number_of_participants) in [0,1]:
            raise HTTPException(
            status_code=403,
            detail= t("invalid-max-number-of-participants")
        )
        
        # validate_date(date_text=competition.start_date)
        # validate_date(date_text=competition.end_date)
        # validate_date(date_text=competition.registration_deadline)

        if (competition.start_date >= competition.end_date):
            raise HTTPException(
            status_code=400,
            detail= t("start-date-greater-end-date")
        )
        if (competition.registration_deadline >= competition.start_date):
            raise HTTPException(
            status_code=400,
            detail= t("registration-deadline-greater-start-date")
        )

        if len(competition.categorie) > 0:
            for category in competition.categorie:
                validate_competition_format(category.competition_format_id)
                if int(category.max_number_of_participants) in [0,1] == True:
                    raise HTTPException(
                    status_code=422,
                    detail= t("invalid-max-number-of-participants")
                ) 
                elif category.max_number_of_participants > competition.max_number_of_participants:
                    raise HTTPException(
                    status_code=422,
                    detail= t("category-max-number-greater-competition-max-number")
                )
                # validate_date(date_text=category.start_date)
                # validate_date(date_text=category.end_date)

                if (category.start_date >= category.end_date):
                    raise HTTPException(
                    status_code=422,
                    detail= t("category-start-date-greater-end-date")
                ) 
                
                if (competition.start_date > category.start_date):
                    raise HTTPException(
                    status_code=422,
                    detail= t("competition-start-date-greater-category-start-date")
                )
                if (competition.end_date < category.end_date):
                    raise HTTPException(
                    status_code=422,
                    detail= t("competition-end-date-less-category-end-date")
                )
                

        competition_uid = str(uuid.uuid4())
        data = {
            "name": competition.name,
            "start_date": competition.start_date,
            "end_date": competition.end_date,
            "sport_id": competition.sport_id,
            "detail_about": competition.detail_about,
            "rules": competition.rules,
            "registration_deadline": competition.registration_deadline,
            "competition_format": competition.competition_format_id,
            "max_number_of_participants": competition.max_number_of_participants,
            "is_deleted": False,
            "date_added": datetime.now(),
            "date_modified": datetime.now(),
            "uid" : competition_uid,
            "registration_number": 0,
            "location": competition.location,
        }
        db.competition.insert_one(data)

        print(f'nombre de categorie : {len(competition.categorie)}')
        if len(competition.categorie) > 0:
            data_cats = []
            for category in competition.categorie:
                cat_uuid = str(uuid.uuid4())
                data_cat = {
                    "type": category.type,
                    "name": category.name,
                    "rules": category.rules,
                    "start_date": category.start_date,
                    "end_date": category.end_date,
                    "competition_format": category.competition_format_id,
                    "max_number_of_participants": category.max_number_of_participants,
                    "uid": str(uuid.uuid4()),
                    "is_deleted": False,
                    "date_added": datetime.now(),
                    "date_modified": datetime.now(),
                    "uid": cat_uuid,
                    "competition_uid": competition_uid,
                    "notation_type": category.notation_type
                }
                data_cats.append(data_cat)
            db.category.insert_many(data_cats)
    
    @classmethod
    def add_a_step_for_a_competition(cls, step: CreateCompetitionStep):
        validate_competition_format(step.competition_format_id)
        # validate_date(date_text=step.start_date)
        # validate_date(date_text=step.end_date)
        if int(step.max_number_of_participants) in [0,1]:
            raise HTTPException(
            status_code=422,
            detail= t("invalid-max-number-of-participants")
        )
        exist_competition = db.competition.find_one(
            {
            "$and": [
            {"uid": step.competition_uid},
            {"is_deleted": False}
            ]
        })
        if not exist_competition:
             raise HTTPException(
            status_code=404,
            detail= t("competition-not-exit")
        )
        if (step.start_date >= step.end_date):
            raise HTTPException(
            status_code=422,
            detail= t("start-date-greater-end-date")
        )
        print(f'start_date exist_comp : {type(exist_competition["start_date"])}')
        if exist_competition["start_date"].replace(tzinfo=utc) > step.start_date.replace(tzinfo=utc):
            raise HTTPException(
            status_code=422,
            detail= t("competition-start-date-greater-step-start-date")
        )
        if step.start_date.replace(tzinfo=utc) > exist_competition["end_date"].replace(tzinfo=utc):
            raise HTTPException(
            status_code=422,
            detail= t("step-start-date-greater-competition-end-date")
        )
        if exist_competition["max_number_of_participants"] < step.max_number_of_participants:
            raise HTTPException(
            status_code=422,
            detail= t("step-max-number-of-participants-greater-comp-max-number-of-participants")
        )
        id_comp = db.competition_step.count_documents({"category_uid": step.category_uid})
        
        stepJson = {
            "uid": str(uuid.uuid4()),
            "is_deleted": False,
            "start_date": step.start_date,
            "end_date": step.end_date,
            "competition_format_id": step.competition_format_id,
            "max_number_of_participants": int(step.max_number_of_participants),
            "status": CompetitionStatus.PENDING,
            "date_added": datetime.now(),
            "date_modified": datetime.now(),
            "category_uid": step.category_uid if step.category_uid else None,
            "competition_uid": exist_competition["uid"],
            "auto_generate_match": step.auto_generate_match,
            "id": id_comp
        }
        db.competition_step.insert_one(stepJson)
    
    @classmethod
    def add_candidate(cls, user_id: str, obj_in: CompetitionRegistration):
        id = db.competition_registration.count_documents({
                "$and": [{"competition_uid": obj_in.competition_uid},\
                                {"$or": 
                                [
                                    {"is_deleted": False}, 
                                    {"is_deleted": None}
                                ]},\
                                {
                                    "step_uid": str(obj_in.step_uid)
                                }                            
                            ]
                        }) + 1
        exsit_competition = db.competition.find_one(
            {
            "$and": [
            {"uid": obj_in.competition_uid},
            {"is_deleted": False}
            ]
        })
        if not exsit_competition:
            raise HTTPException(
                status_code=404,
                detail= t("competition-not-exit")
            )
        if not db.competition_step.find_one(
            {
            "$and": 
            [
                {"uid": obj_in.step_uid},
                {"competition_uid": obj_in.competition_uid},
                {"is_deleted": False}
            ]
        }):
            raise HTTPException(
                status_code=404,
                detail= t("competition-step-not-exit")
            )
        if obj_in.candidate:
            if len(obj_in.candidate) + exsit_competition["registration_number"] > exsit_competition["max_number_of_participants"]:
                d = exsit_competition["max_number_of_participants"] - exsit_competition["registration_number"]
                raise HTTPException(
                    status_code=422,
                    detail= f'{t("invalid-canditure-number")}{d}'
                )
            for cand in obj_in.candidate:
                if not validate_email(cand.email):
                    raise HTTPException(
                        status_code=422,
                        detail= t("invalid-email")
                    )
                if is_valid_phonenumber(cand.phonenumber) == False:
                    raise HTTPException(
                        status_code=422,
                        detail= t("invalid-phonenumber")
                    )
                if cand.gender_id not in [1,2,3] :
                    raise HTTPException(
                    status_code=422,
                    detail= t("invalid-user-sex")
                )
        if obj_in.candidate_uid and len(obj_in.candidate_uid)>0:
            if len(obj_in.candidate_uid) + exsit_competition["registration_number"] > exsit_competition["max_number_of_participants"]:
                d = exsit_competition["max_number_of_participants"] - exsit_competition["registration_number"]
                raise HTTPException(
                    status_code=422,
                    detail= f'{t("invalid-canditure-number")}{d}'
                )
                
        if  not obj_in.candidate and len(obj_in.candidate_uid) > 0:
            datas = []
            n_id = len(obj_in.candidate_uid) + id -1
            for obj in obj_in.candidate_uid:
                if not db.user.find_one({"public_id": obj}):
                    raise HTTPException(
                        status_code=404,
                        detail= f'{t("user-not-exist")}: {obj}'
                    )
                data= {
                    "uid": str(uuid.uuid4()),
                    "candidate_uid": str(obj),
                    "competition_uid": str(obj_in.competition_uid),
                    "step_uid": str(obj_in.step_uid),
                    "author_id": user_id,
                    "is_deleted": False,
                    "date_added": datetime.now(),
                    "date_modified": datetime.now(),
                    "id": n_id
                }
                datas.append(data)
                n_id -= 1
            db.competition_registration.insert_many(datas)
            db.competition.update_one(
                    {"uid": obj_in.competition_uid},
                    update= {"$inc": {
                        "registration_number": len(datas)
                    }},
                    upsert=False
                )
        else:
            users= []
            registrations= []
            n_id = len(obj_in.candidate) + id - 1
            for cand in obj_in.candidate:
                public_id = str(uuid.uuid4())
                user = {
                        "password": get_password_hash(str(cand.password)),
                        "email": cand.email,
                        "firstname": cand.firstname,
                        "phonenumber": cand.phonenumber,
                        "lastname": cand.lastname,
                        "role_id": 5,
                        "gender_id": cand.gender_id,
                        "public_id": public_id,
                        "is_deleted": False,
                        "date_added": datetime.now(),
                        "date_modified": datetime.now()
                    }
                data= {
                    "uid": str(uuid.uuid4()),
                    "candidate_uid": public_id,
                    "competition_uid": str(obj_in.competition_uid),
                    "step_uid": str(obj_in.step_uid),
                    "date_added": datetime.now(),
                    "author_id": user_id,
                    "is_deleted": False,
                    "date_added": datetime.now(),
                    "date_modified": datetime.now(),
                    "id": n_id
                }
                registrations.append(data)
                users.append(user)
                n_id -= 1
                
            db.user.insert_many(users)
            db.competition_registration.insert_many(registrations)
            db.competition.update_one(
                    {"uid": obj_in.competition_uid},
                    update= {"$inc": {
                        "registration_number": len(registrations)
                    }},
                    upsert=False
                )
    
    @classmethod
    def list_competition_candidates(cls, competition_uid: str):
        try:
            res = []
            candidates = db.competition_registration.find({
                "$and": [{"competition_uid": competition_uid},\
                                {"$or": 
                                [
                                    {"is_deleted": False}, 
                                    {"is_deleted": None}
                                ]}                              
                            ]
                        })
            for candidate in candidates:
                print(f'-------->{dict(candidate)}')
                res.append({
                    "id": candidate["id"] if "id" in candidate else 0,
                    "candidature_uid": candidate["uid"] if "uid" in candidate and candidate["uid"] else "",
                    "is_deleted": candidate["is_deleted"] if "is_deleted" in candidate and candidate["is_deleted"] else None,
                    "date_added": candidate["date_added"] if "date_added" in candidate and candidate["date_added"] else None,
                    "date_modified": candidate["date_modified"] if "date_modified" in candidate and candidate["date_modified"] else None,
                    "user": userEntity(db.user.find_one({"$and": [
                    {"public_id": candidate["candidate_uid"]},
                    {"$or": 
                        [
                            {"is_deleted": False}, 
                            {"is_deleted": None}
                    ]}]})
                )})
            return res
        except Exception as e:
                print(str(e))
                raise HTTPException(
                status_code=500,
                detail= f'{t("server-error")} : {e}'
            )
    
    @classmethod
    def cancel_a_candidature(cls, candidature_uid: str):
        exist_registration = db.competition_registration.find_one({"$and": [{"uid": candidature_uid}, {"$or": [{"is_deleted": False},{"is_deleted": None}]}]})
        if not exist_registration:
            raise HTTPException(
                status_code=404,
                detail= t("application-not-found")
            )
        db.competition_registration.update_one(
            {"uid": candidature_uid},
            update={"$set": {"is_deleted": True}}
        )
        db.competition.update_one(
                    {"uid": exist_registration["competition_uid"]},
                    update= {"$inc": {
                        "registration_number": -1
                    }},
                    upsert=False
                )
    @classmethod
    def close_competition_step(cls, step_uid: str):
        exist_step = db.competition_step.find_one({"$and": [{"uid": step_uid}, {"$or": [{"is_deleted": False},{"is_deleted": None}]}]})
        if not exist_step:
            raise HTTPException(
                status_code=404,
                detail= t("step-not-found")
            )
        db.competition_step.update_one(
            {"uid": step_uid},
            update={"$set": {"status": CompetitionStatus.FINISHED}},
            upsert=False
        )
    
    @classmethod
    def cancel_competition_step(cls, step_uid: str):
        exist_step = db.competition_step.find_one({"$and": [{"uid": step_uid}, {"$or": [{"is_deleted": False},{"is_deleted": None}]}]})
        if not exist_step:
            raise HTTPException(
                status_code=404,
                detail= t("step-not-found")
            )
        db.competition_step.update_one(
            {"uid": step_uid},
            update={"$set": {"status": CompetitionStatus.CANCELED}},
            upsert=False
        )
        
            
