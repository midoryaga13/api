from typing import Generic, TypeVar
import uuid
from fastapi import HTTPException
from pydantic import BaseModel
from config.database import db
from core.i18n import t
from core.security import get_password_hash
from models.user import RegisterSimpleUser, RegisterUser, UpdateUser, User
from schemas.user import defaultEntity, userEntity


from utils.helper import is_valid_phonenumber, upload_file, validate_email

CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)

class CRUDBase(Generic[CreateSchemaType, UpdateSchemaType]):
    
    def __init__(self):
        """
        CRUD object with default methods to Create, Read, Update, Delete (CRUD).
        **Parameters**
        * `schema`: A Pydantic model (schema) class
        """

    def get_by_mail(email: str):
        try:
            user = db.user.find_one({"email": email})
            if not user:
                raise HTTPException(
                    status_code=404,
                    detail= t("user-not-exist")
                )
            return userEntity(user)
        except Exception as e:
            raise HTTPException(
                    status_code=500,
                    detail= t("server-error")
                )
    
    def get_by_public_id(id: str):
        try:
            user = db.user.find_one({"public_id": id})
            if not user:
                raise HTTPException(
                    status_code=404,
                    detail= t("user-not-exist")
                )
            return userEntity(user)
        except Exception as e:
            raise HTTPException(
                    status_code=500,
                    detail= t("server-error")
                )

    def get_multi(
        *, page: int = 0, per_page: int = 10,
        user: User
    ):
        try:
            result = []
            users = db.user.find({}).limit(per_page)
            if user["role_id"] == 3:
                users = db.user.find({"role_id": 4}).limit(per_page)
            
            print("---> result : {}".format(type(users)))
            if users:
                for doc in users:
                    result.append(userEntity(doc))
            return result
        except Exception as e:
            print(str(e))
            raise HTTPException(
            status_code=500,
            detail= t("server-error")
            )

    def create(*, user: RegisterUser):
        if not validate_email(user.email):
            raise HTTPException(
            status_code=401,
            detail= t("invalid-email")
        )
        if user.role_id not in [1,2,3,4,5] :
            raise HTTPException(
            status_code=422,
            detail= t("invalid-user-role")
        )
        if user.gender_id not in [1,2,3] :
            raise HTTPException(
            status_code=422,
            detail= t("invalid-user-sex")
        )
        if is_valid_phonenumber(user.phonenumber) == False:
            raise HTTPException(
                status_code=422,
                detail= t("invalid-phonenumber")
            )
        exist_user = db.user.find_one({"email": user.email})
        if not  exist_user:
            data = {
                "password": get_password_hash(str(user.password)),
                "email": user.email,
                "firstname": user.firstname,
                "phonenumber": user.phonenumber,
                "lastname": user.lastname,
                "role_id": user.role_id,
                "gender_id": user.gender_id,
                "public_id": str(uuid.uuid4()),
            }  
            db.user.insert_one(dict(data))
        else:
            raise HTTPException(
            status_code=403,
            detail= t("email-already-used")
        )

    def update(
        *,
        user_id: str,
        obj_in: UpdateUser
    ):
        try:
            db.user.find_one_and_update(
                {"public_id": user_id},
                { "$set": dict(obj_in)}
                )
            user = db.user.find_one({"public_id": user_id})
            return userEntity(user)
        except Exception as e:
                print(str(e))
                raise HTTPException(
                status_code=500,
                detail= t("server-error")
            )

    def remove(*, id: str):
        try:
            db.user.find_one_and_update({"public_id": id}, {"$set": {"is_deleted": True}})
            return {"detail" : t("user-delete-success")}
        except Exception as e:
                print(str(e))
                raise HTTPException(
                status_code=500,
                detail= t("server-error")
            )
    def get_default_data():
        try:
            roles_dict = []
            sports_dict = []
            comps= []
            genders_dict= []
            criterias = []

            roles = db.roles.find()
            sports = db.sports.find()
            competition_formats = db.competition_formats.find()
            genders = db.genders.find()
            score_criteria = db.score_criteria.find()
            if roles : 
                for role in roles:
                    print(role)
                    roles_dict.append(defaultEntity(role))
            if sports:
                for sport in sports:
                    sports_dict.append(defaultEntity(sport))
            if competition_formats:
                for comp in competition_formats:
                    comps.append(defaultEntity(comp))
            if genders:
                for gender in genders:
                    genders_dict.append(defaultEntity(gender))
            if score_criteria:
                for criteria in score_criteria:
                    criterias.append(defaultEntity(criteria))
            datas = {
                "roles": roles_dict,
                "sports":sports_dict,
                "competition_formats": comps,
                "genders":  genders_dict,
                "score_criteria": criterias
            }
            return datas
        except Exception as e:
                print(str(e))
                raise HTTPException(
                status_code=500,
                detail= t("server-error")
            )
    
    def create_simple_user(*, user:RegisterSimpleUser):
        if not validate_email(user.email):
            raise HTTPException(
            status_code=401,
            detail= t("invalid-email")
        )
        # if user.role_id not in [1,2,3,4,5] :
        #     raise HTTPException(
        #     status_code=422,
        #     detail= t("invalid-user-role")
        # )
        if user.gender_id not in [1,2,3] :
            raise HTTPException(
            status_code=422,
            detail= t("invalid-user-sex")
        )
        if is_valid_phonenumber(user.phonenumber) == False:
            raise HTTPException(
                status_code=422,
                detail= t("invalid-phonenumber")
            )
        exist_user = db.user.find_one({"email": user.email})
        if not  exist_user:
            data = {
                "password": get_password_hash(str(user.password)),
                "email": user.email,
                "firstname": user.firstname,
                "phonenumber": user.phonenumber,
                "lastname": user.lastname,
                "role_id": 5,
                "gender_id": user.gender_id,
                "public_id": str(uuid.uuid4()),
            }  
            db.user.insert_one(dict(data))
        else:
            raise HTTPException(
            status_code=403,
            detail= t("email-already-used")
        )
