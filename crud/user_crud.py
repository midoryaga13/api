from fastapi import HTTPException
from core.i18n import t
from core.security import verify_password
from crud.base import CRUDBase
from config.database import db


class CRUDUser(CRUDBase):

    def authenticate(*, email: str, password: str):
        try:
            user = user = db.user.find_one({"$and": [{"email": email}, {"$or": [{"is_deleted": False}, {"is_deleted": None}]}]})
            if not user:
                return None
            if not verify_password(password, user["password"] or ""):
                return None
            return user
        except Exception as e:
                print(str(e))
                raise HTTPException(
                status_code=500,
                detail= t("server-error")
            )

user = CRUDUser()