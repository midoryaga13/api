
from datetime import datetime
from typing import Generic
import uuid

from fastapi import HTTPException
from core.i18n import t

from crud.base import CreateSchemaType, UpdateSchemaType
from models.match import MatchStatus
from models.voting import ScoringModel
from config.database import db

class CRUDVote(Generic[CreateSchemaType, UpdateSchemaType]):
    
    @classmethod
    def register_a_score(cls, data: ScoringModel, user_id: str):
        if not db.competition_registration.find_one(
            {
                "$and": [
                    {"uid": data.candidate_uid}, 
                    {"$or": [
                        {"is_deleted": False}, 
                        {"is_deleted": None}
                        ]}
                    ]}):
            raise HTTPException(
                status_code=404,
                detail= t("application-not-found")
            )
        exist_match = db.match.find_one(
            {"$and": [
                {"uid": data.match_uid},
                {"$or": [
                    {"is_deleted": False}, 
                    {"is_deleted": None}
                ]}
            ]})
        if not exist_match:
                raise HTTPException(
                status_code=404,
                detail= t("match-not-exit")
            )
        if exist_match["status"] != MatchStatus.PENDING:
            raise HTTPException(
                status_code=422,
                detail= t("match-not-activated")
            )
        if data.round_id > exist_match["round_number"]:
            raise HTTPException(
                status_code=422,
                detail= t("invalid-round-id")
            )
        for criteria in data.voting_scores:
            if not db.score_criteria.find_one({"id": criteria.criteria_id}):
                raise HTTPException(
                status_code=404,
                detail= t("criteria-not-found")
            )
        voting_score = []
        total_score = 0
        for criteria in data.voting_scores:
            total_score += criteria.avarage
            voting_score.append({
                "criteria_id": criteria.criteria_id,
                "avarage": criteria.avarage,
            })
        uid = str(uuid.uuid4())
        data = {
            "uid": uid,
            "candidate_uid": data.candidate_uid,
            "match_uid": data.match_uid,
            "round_id": data.round_id,
            "voting_scores": voting_score,
            "author": user_id,
            "date_added": datetime.now(),
            "total_avarage": total_score / len(data.voting_scores) 
        }
        db.notation.insert_one(data)
        exist_notations = db.notation.find({"candidate_uid": data.candidate_uid, "match_uid": data.match_uid})
        match_total_avarage = 0
        for notation in exist_notations:
            match_total_avarage += notation["total_avarage"]/ len(exist_match["juries"])
        notation_match = {
            "candidate_uid":data.candidate_uid,
            "avarage": match_total_avarage,
            "round_id": data.round_id
        }
        db.match.update_one(
            {"$and": [{"uid": data.match_uid}, {"$or": [{"is_deleted": False}, {"is_deleted": None}]}]},
            update= {
                "score": [notation_match, *exist_match["score"]]
            },
            upsert=False
        )
        current_match = db.match.find_one(
            {"$and": [
                {"uid": data.match_uid},
                {"$or": [
                    {"is_deleted": False}, 
                    {"is_deleted": None}
                ]}
            ]})
        if (data.round_id == exist_match["round_number"]) and ( len(current_match["score"]) == (len(exist_match["juries"]) * len(exist_match["participant_uids"]))):
            winner = notation_match["candidate_uid"]
            for sc in current_match["score"]:
                if sc["avarage"] > notation_match["avarage"]:
                    winner = sc["candidate_uid"]
            db.match.update_one(
            {"$and": [{"uid": data.match_uid}, {"$or": [{"is_deleted": False}, {"is_deleted": None}]}]},
            update= {
                "winner": winner
            },
            upsert=False
            )
        #     team = {
        #     "round": raundNo,
        #     "participant_uids": tabOne+tabTwo,
        #     "bracketNo": groupeId,
        #     "score": [],
        #     "round_number": match.round_number,
        #     "date_added": datetime.now(),
        #     "uid": uid, 
        #     "competition_step_uid": match.competition_step_uid,
        #     "date_modified": datetime.now(),
        #     "author_id": user_id,
        #     "status": MatchStatus.REGISTERED,
        #     "duration": match.duration,
        #     "is_deleted": False,
        #     "start_date": match.start_date
        # }

            
