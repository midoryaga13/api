
from datetime import datetime
import math
from typing import Generic
import uuid

from fastapi import HTTPException
import pytz
from core.i18n import t

from crud.base import CreateSchemaType, UpdateSchemaType
from models.match import ActivateMatch, CreateMatch, MatchStatus, UpdateMatch
from config.database import db
from schemas.user import userEntity
from utils.helper import validate_date

def tournament_round( no_of_teams , matchlist ):
    new_matches = []
    for team_or_match in matchlist:
        if type(team_or_match) == type([]):
            new_matches += [ tournament_round( no_of_teams, team_or_match )]
        else:
            new_matches += [ [ team_or_match, no_of_teams + 1 - team_or_match ] ]
    return new_matches

def flatten_list( matches ):
    teamlist = []
    for team_or_match in matches:
        if type(team_or_match) == type([]):
            teamlist += flatten_list( team_or_match )
        else:
            teamlist += [team_or_match]
    return teamlist

def generate_tournament_schuffle_list( num ):
    num_rounds = math.log( num, 2 )
    if num_rounds != math.trunc( num_rounds ):
        raise ValueError( "Number of teams must be a power of 2" )
    teams = 1
    result = [1]
    while teams != num:
        teams *= 2
        result = tournament_round( teams, result )
    return flatten_list( result )

def generate_tournament_shuffle_list_group(tabList: list, candidates: any, match: CreateMatch, user_id: str)->list:
    i=0
    brackets = []
    raundNo = 1
    while i< len(tabList):
        
        if len(tabList) == 2:
            groupeId = len(tabList) /2
        elif len(tabList) == 4:
            if i < 2:
                groupeId = len(tabList) /4
            else:
                groupeId = len(tabList) /2
        elif len(tabList) == 8:
            if i < 4:
                if i<2:
                    groupeId = len(tabList) / 8
                else:
                    groupeId = len(tabList) / 4
            else:
                if i<6: groupeId = math.log(len(tabList), 2)
                else: groupeId = math.log(len(tabList), 2)+1
        elif len(tabList) == 16:
            if i < 8:
                if i < 4:
                    if i<2:
                        groupeId = len(tabList) / 16
                    else:
                        groupeId = len(tabList) / 8
                else:
                    print(i)
                    if i<6: groupeId = math.log(len(tabList), 2)-1
                    elif i<8: groupeId = math.log(len(tabList), 2)
            else:
                if i<10:groupeId = math.log(len(tabList), 2)+1
                elif i<12:groupeId = math.log(len(tabList), 2)+2
                elif i<14:groupeId = math.log(len(tabList), 2)+3
                elif i<16:groupeId = math.log(len(tabList), 2)+4
        elif len(tabList) == 32:
            if i < 16:
                if i < 8:
                    if i < 4:
                        if i<2:
                            groupeId = len(tabList) / 32
                        else:
                            groupeId = len(tabList) / 16
                    else:
                        print(i)
                        if i<6: groupeId = math.log(8, 2)
                        elif i<8: groupeId = math.log(16, 2)
                else:
                    if i<10:groupeId = math.log(len(tabList), 2)
                    elif i<12:groupeId = math.log(len(tabList), 2)+1
                    elif i<14:groupeId = math.log(len(tabList), 2)+2
                    elif i<16:groupeId = math.log(len(tabList), 2)+3
            else: 
                if i<24:
                    if i <18:
                        groupeId = math.log(len(tabList), 2)+4
                    else:
                        if i<20:groupeId = math.log(len(tabList), 2)+5
                        else:
                            if i<22:groupeId = math.log(len(tabList), 2)+6
                            else:
                                if i<24: groupeId = math.log(len(tabList), 2)+7
                else:
                    if i<26:
                        groupeId = math.log(len(tabList), 2)+8
                    else:
                        if i<28:groupeId = math.log(len(tabList), 2)+9
                        else:
                            if i<30:groupeId = math.log(len(tabList), 2)+10
                            else:
                                if i<32:groupeId = math.log(len(tabList), 2)+11
        tabOne = [d["uid"] for d in candidates if tabList[i]== d["id"]]
        tabTwo = [d["uid"] for d in candidates if tabList[i+1]== d["id"]]
        uid = str(uuid.uuid4())
        team = {
            "round": raundNo,
            "participant_uids": tabOne+tabTwo,
            "bracketNo": groupeId,
            "score": [],
            "round_number": match.round_number,
            "date_added": datetime.now(),
            "uid": uid, 
            "competition_step_uid": match.competition_step_uid,
            "date_modified": datetime.now(),
            "author_id": user_id,
            "status": MatchStatus.REGISTERED,
            "duration": match.duration,
            "is_deleted": False,
            "start_date": match.start_date
        }
        db.match.insert_one(team)
        brackets.append(uid)
        i+=2
    continue_genaration = len(brackets)
    while continue_genaration % 2 == 0:
        raundNo += 1
        continue_genaration = continue_genaration /2
        i = continue_genaration
        while i>0:
            uid = str(uuid.uuid4())
            team = {
            "round": raundNo,
            "participant_uids": [],
            "bracketNo": groupeId+1,
            "score": [],
            "round_number": match.round_number,
            "date_added": datetime.now(),
            "uid": uid, 
            "competition_step_uid": match.competition_step_uid,
            "date_modified": datetime.now(),
            "author_id": user_id,
            "status": MatchStatus.REGISTERED,
            "duration": match.duration,
            "is_deleted": False,
            }
            db.match.insert_one(team)
            brackets.append(uid)
            i -= 1
    return brackets

utc=pytz.UTC

class CRUDMatch(Generic[CreateSchemaType, UpdateSchemaType]):
    
    @classmethod
    def create_match(cls, match: CreateMatch, user_id: str):
        exist_step = db.competition_step.find_one({"uid": match.competition_step_uid})
        if not exist_step:
            raise HTTPException(
            status_code=404,
            detail= t("competition-step-not-exit")
        )
        if len(match.participant_uids) < 2:
            raise HTTPException(
            status_code=422,
            detail= t("invalid-participant_number")
        )
        if match.duration <= 0:
            raise HTTPException(
            status_code=422,
            detail= t("invalid-match-duration")
        )
        # validate_date(date_text=match.start_date)
        if exist_step["start_date"].replace(tzinfo=utc) > match.start_date.replace(tzinfo=utc):
            raise HTTPException(
            status_code=422,
            detail= t("step-start-date-greater-match-start-date")
        ) 
        if match.start_date.replace(tzinfo=utc) > exist_step["end_date"].replace(tzinfo=utc):
            raise HTTPException(
            status_code=422,
            detail= t("match-start-date-greater-step-start-date")
        )
        for cand in match.participant_uids:
            if not db.competition_registration.find_one({"uid": cand}):
                raise HTTPException(
                status_code=404,
                detail= t("candidature-not-exit")
            )
        
        data = {
            "participant_uids": match.participant_uids,
            "competition_step_uid": match.competition_step_uid,
            "start_date": validate_date(date_text=match.start_date),
            "duration": match.duration,
            "is_deleted": False,
            "uid": str(uuid.uuid4()),
            "author_id": user_id,
            "date_added": datetime.now(),
            "date_modified": datetime.now(),
            "round_number": match.round_number,
            "status": MatchStatus.REGISTERED,
            "score": []
        }
        db.match.insert_one(data)
    
    @classmethod
    def update_a_match(cls, match: UpdateMatch, user_id: str):
        if not db.match.find_one({"$and": [{"uid": match.uid}, {"is_deleted": False}]}):
                raise HTTPException(
                status_code=404,
                detail= t("match-not-exit")
            )
        exist_step = db.competition_step.find_one({"uid": match.competition_step_uid})
        if not exist_step:
            raise HTTPException(
            status_code=404,
            detail= t("competition-step-not-exit")
        )
        if len(match.participant_uids) < 2:
            raise HTTPException(
            status_code=422,
            detail= t("invalid-participant_number")
        )
        if match.duration <= 0:
            raise HTTPException(
            status_code=422,
            detail= t("invalid-match-duration")
        )
        # validate_date(date_text=match.start_date)
        if exist_step["start_date"].replace(tzinfo=utc) > match.start_date.replace(tzinfo=utc):
            raise HTTPException(
            status_code=422,
            detail= t("step-start-date-greater-match-start-date")
        ) 
        if match.start_date.replace(tzinfo=utc) > exist_step["end_date"].replace(tzinfo=utc):
            raise HTTPException(
            status_code=422,
            detail= t("match-start-date-greater-step-start-date")
        )
        for cand in match.participant_uids:
            if not db.competition_registration.find_one({"uid": cand}):
                raise HTTPException(
                status_code=404,
                detail= t("candidature-not-exit")
            )
        obj_in= {}

        if match.participant_uids:
            obj_in["participant_uids"]= match.participant_uids
        if match.competition_step_uid:
            obj_in["competition_step_uid"]= match.competition_step_uid
        if match.start_date:
            obj_in["start_date"]= match.start_date
        if match.duration:
            obj_in["duration"]= match.duration
        if match.round_number:
            obj_in["round_number"]= match.round_number
        if match.status:
            obj_in["status"]= match.status
        obj_in["date_modified"]= datetime.now()
        
        db.match.update_one(
            {"$and": [{"uid": match.uid}, {"is_deleted": False}]},
            update={"$set": obj_in},
            upsert=False
        )
    
    @classmethod
    def cancel_a_match(cls, match_uid: str):
        if not db.match.find_one({"$and": [{"uid": match_uid}, {"is_deleted": False}]}):
                raise HTTPException(
                status_code=404,
                detail= t("match-not-exit")
            )
        db.match.update_one(
            {"$and": [{"uid": match_uid}, {"is_deleted": False}]},
            update={"$set": {"is_deleted": True}},
            upsert=False
        )

    @classmethod
    def get_matchs(cls, step_uid: str):
        res= []
        exist_matchs = db.match.find({
            "$and": [
                {"competition_step_uid": step_uid},
                {"$or":[
                    {"is_deleted": False}, 
                    {"is_deleted": None}
                    ]}
                ]
            })
        for match in exist_matchs:
            participants = []
            jurie_tab = []
            if "participant_uids" in match and match["participant_uids"]:
                for candidate_uid in match["participant_uids"]:
                    candidate = db.competition_registration.find_one({"$and": [{"uid": candidate_uid},\
                                    {"$or": [{"is_deleted": False},
                                    {"is_deleted": None
                                    }]
                                }]
                            })
                    obj = {
                        "id": candidate["id"] if "id" in candidate else 0,
                        "uid": candidate["uid"] if "uid" in candidate else None,
                        "candidature_uid": candidate["uid"] if "uid" in candidate and candidate["uid"] else "",
                        "is_deleted": candidate["is_deleted"] if "is_deleted" in candidate and candidate["is_deleted"] else None,
                        "date_added": candidate["date_added"] if "date_added" in candidate and candidate["date_added"] else None,
                        "date_modified": candidate["date_modified"] if "date_modified" in candidate and candidate["date_modified"] else None,
                        "user": userEntity(db.user.find_one({"$and": [
                        {"public_id": candidate["candidate_uid"]},
                        {"$or": 
                            [
                                {"is_deleted": False}, 
                                {"is_deleted": None}
                        ]}]})
                    )}
                    participants.append(obj)
            if "juries" in match and match["juries"]:
                for jury_uid in match["juries"]:
                    juge = userEntity(db.user.find_one({"$and": [
                    {"public_id": jury_uid},
                    {"$or": 
                        [
                            {"is_deleted": False}, 
                            {"is_deleted": None}
                    ]}]}))
                    jurie_tab.append(juge)

            obj_in = {
                "round": match["round"] if "round" in match else None,
                "participant_uids": participants,
                "bracketNo": match["bracketNo"] if "bracketNo" in match else None,
                "score": match["score"] if "score" in match else None,
                "round_number": match["round_number"] if "round_number" in match else None,
                "date_added": match["date_added"] if "date_added" in match else None,
                "uid": match["uid"] if "uid" in match else None, 
                "competition_step_uid": match["competition_step_uid"] if "competition_step_uid" in match else None,
                "date_modified": match["date_modified"] if "date_modified" in match else None,
                "author_id": userEntity(db.user.find_one({"$and": [
                    {"public_id": match["author_id"]},
                    {"$or": 
                        [
                            {"is_deleted": False}, 
                            {"is_deleted": None}
                    ]}]})
                ),
                "status": match["status"] if "status" in match else None,
                "duration": match["duration"] if "duration" in match else None,
                "is_deleted": match["is_deleted"] if "is_deleted" in match else None,
                "start_date": match["start_date"] if "start_date" in match else None,
                "juries": jurie_tab
            }
            res.append(obj_in)
        return res
    
    @classmethod
    def auto_generate_matchs(cls, user_id: str, match: CreateMatch):
        exist_step = db.competition_step.find_one({"uid": match.competition_step_uid})
        if not exist_step:
            raise HTTPException(
            status_code=404,
            detail= t("competition-step-not-exit")
        )
        if match.duration <= 0:
            raise HTTPException(
            status_code=422,
            detail= t("invalid-match-duration")
        )
        # validate_date(date_text=match.start_date)
        if exist_step["start_date"].replace(tzinfo=utc) > match.start_date.replace(tzinfo=utc):
            raise HTTPException(
            status_code=422,
            detail= t("step-start-date-greater-match-start-date")
        ) 
        if match.start_date.replace(tzinfo=utc) > exist_step["end_date"].replace(tzinfo=utc):
            raise HTTPException(
            status_code=422,
            detail= t("match-start-date-greater-step-start-date")
        )
        exist_candidate = db.competition_registration.find({"$and": [{"step_uid": match.competition_step_uid},\
                                 {"$or": [{"is_deleted": False},
                                 {"is_deleted": None
                                 }]
                            }]
                        })
        nbr_candidate = db.competition_registration.count_documents({"$and": [{"step_uid": match.competition_step_uid},\
                                 {"$or": [{"is_deleted": False},
                                 {"is_deleted": None
                                 }]
                            }]
                        })
        if exist_candidate:
            res= []
            if nbr_candidate not in [2, 4, 8, 16, 32]:
                raise HTTPException(
                    status_code=422,
                    detail= t("total-candidate-number-not-matched")
                )
            for candidate in exist_candidate:
                res.append({
                    "id": candidate["id"] if "id" in candidate else 0,
                    "uid": candidate["uid"] if "uid" in candidate else None,
                    "candidature_uid": candidate["uid"] if "uid" in candidate and candidate["uid"] else "",
                    "is_deleted": candidate["is_deleted"] if "is_deleted" in candidate and candidate["is_deleted"] else None,
                    "date_added": candidate["date_added"] if "date_added" in candidate and candidate["date_added"] else None,
                    "date_modified": candidate["date_modified"] if "date_modified" in candidate and candidate["date_modified"] else None,
                    "user": userEntity(db.user.find_one({"$and": [
                    {"public_id": candidate["candidate_uid"]},
                    {"$or": 
                        [
                            {"is_deleted": False}, 
                            {"is_deleted": None}
                    ]}]})
                )})
            tab = generate_tournament_schuffle_list(nbr_candidate)
            tabList = generate_tournament_shuffle_list_group(tab, res, match, user_id)
            data = {
               "step_uid": match.competition_step_uid,
               "tree_one":tabList,
               "author_id": user_id
            }
            db.competition_step_trees.insert_one(data)
            return "ok"
        else:
            raise HTTPException(
                    status_code=404,
                    detail= t("not_enought_candidate")
                )

    @classmethod
    def activate_match(cls, obj_in: ActivateMatch):
        if not db.match.find_one({
            "$and":[
                {"uid": obj_in.match_id}, 
                {"$or": 
                    [
                        {"is_deleted": False}, 
                        {"is_deleted": None}
                    ]
                }
            ]}):
            raise HTTPException(
            status_code=404,
            detail= t("match-not-exit")
        )

        db.match.update_one({
            "$and":[
                {"uid": obj_in.match_id}, 
                {"$or": 
                    [
                        {"is_deleted": False}, 
                        {"is_deleted": None}
                    ]
                }
            ]
        },
        update={"$set": {
            "status": MatchStatus.PENDING,
            "juries": obj_in.jury_uids
        }},
        upsert=False
        )
        # TODO notify the jury with push notification
        
            
        
