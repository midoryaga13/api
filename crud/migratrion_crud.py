
import json
import os
from fastapi import HTTPException
from core.config import Config
from config.database import db

from core.i18n import t

current_dir = os.path.dirname(os.path.realpath(__file__))

class CRUDMigration():

    @classmethod
    def default_user_role(cls, access_key: str):
        try:
            if not access_key  or access_key is None:
                 raise HTTPException(
                status_code=401,
                detail= t("access-denied")
            )
            if access_key and access_key != Config.ACCESS_ADMIN_KEY:
                print("key not matched")
                raise HTTPException(
                status_code=500,
                detail= t("invalid-access-key")
            )
            with open('{}/default_datas/roles.json'.format(current_dir)) as f:
                datas = json.load(f)
                for data in datas:
                    user_role = db.roles.find_one({"id": data["id"]})
                    if user_role:
                        db.roles.update_one({"id": user_role["id"]}, {"$set": {"title_en":data["title_en"], "title_fr": data["title_fr"]}})
                    else:
                        db.roles.insert_one(dict(data))
        except Exception as e:
            print(str(e))
            raise HTTPException(
                status_code=500,
                detail= t("server-error")
            )
    @classmethod
    def default_sport(cls, access_key: str):
        try:
            if not access_key  or access_key is None:
                 raise HTTPException(
                status_code=401,
                detail= t("access-denied")
            )
            if access_key and access_key != Config.ACCESS_ADMIN_KEY:
                print("key not matched")
                raise HTTPException(
                status_code=500,
                detail= t("invalid-access-key")
            )
            with open('{}/default_datas/sports.json'.format(current_dir)) as f:
                datas = json.load(f)
                for data in datas:
                    user_role = db.sports.find_one({"id": data["id"]})
                    if user_role:
                        db.sports.update_one({"id": user_role["id"]}, {"$set": {"title_fr": data["title_fr"]}})
                    else:
                        db.sports.insert_one(dict(data))
        except Exception as e:
            print(str(e))
            raise HTTPException(
                status_code=500,
                detail= t("server-error")
            )
    
    @classmethod
    def default_competition_format(cls, access_key: str):
        try:
            if not access_key  or access_key is None:
                 raise HTTPException(
                status_code=401,
                detail= t("access-denied")
            )
            if access_key and access_key != Config.ACCESS_ADMIN_KEY:
                print("key not matched")
                raise HTTPException(
                status_code=500,
                detail= t("invalid-access-key")
            )
            with open('{}/default_datas/competition_format.json'.format(current_dir)) as f:
                datas = json.load(f)
                for data in datas:
                    user_role = db.competition_formats.find_one({"id": data["id"]})
                    if user_role:
                        db.competition_formats.update_one({"id": user_role["id"]}, {"$set": {"title_en":data["title_en"], "title_fr": data["title_fr"]}})
                    else:
                        db.competition_formats.insert_one(dict(data))
        except Exception as e:
            print(str(e))
            raise HTTPException(
                status_code=500,
                detail= t("server-error")
            )

    @classmethod
    def default_gender(cls, access_key: str):
        try:
            if not access_key  or access_key is None:
                 raise HTTPException(
                status_code=401,
                detail= t("access-denied")
            )
            if access_key and access_key != Config.ACCESS_ADMIN_KEY:
                print("key not matched")
                raise HTTPException(
                status_code=500,
                detail= t("invalid-access-key")
            )
            with open('{}/default_datas/gender.json'.format(current_dir)) as f:
                datas = json.load(f)
                for data in datas:
                    user_role = db.genders.find_one({"id": data["id"]})
                    if user_role:
                        db.genders.update_one({"id": user_role["id"]}, {"$set": {"title_en":data["title_en"], "title_fr": data["title_fr"]}})
                    else:
                        db.genders.insert_one(dict(data))
        except Exception as e:
            print(str(e))
            raise HTTPException(
                status_code=500,
                detail= t("server-error")
            )
    
    @classmethod
    def default_score_criteria(cls, access_key: str):
        try:
            if not access_key  or access_key is None:
                 raise HTTPException(
                status_code=401,
                detail= t("access-denied")
            )
            if access_key and access_key != Config.ACCESS_ADMIN_KEY:
                print("key not matched")
                raise HTTPException(
                status_code=422,
                detail= t("invalid-access-key")
            )
            with open('{}/default_datas/vote_criteria.json'.format(current_dir)) as f:
                datas = json.load(f)
                for data in datas:
                    score_criteria = db.score_criteria.find_one({"id": data["id"]})
                    if score_criteria:
                        db.score_criteria.update_one({"id": score_criteria["id"]}, {"$set": {"title_en":data["title_en"], "title_fr": data["title_fr"]}})
                    else:
                        db.score_criteria.insert_one(dict(data))
        except Exception as e:
            print(str(e))
            raise HTTPException(
                status_code=500,
                detail= f'{t("server-error")} : {e}'
            )