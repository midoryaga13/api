from datetime import datetime
from typing import List
# from bson.raw_bson import RawBSONDocument
from pydantic import BaseModel

def MatchEntity(item: any)-> dict:
    return {
        "participant_uids": item["participant_uids"] if "participant_uids" in item and item["participant_uids"] else None,
        "competition_step_uid": item["competition_step_uid"] if "competition_step_uid" in item and item["competition_step_uid"] else None,
        "start_date": item["start_date"] if "start_date" in item and item["start_date"] else None,
        "duration": item["duration"] if "duration" in item and item["duration"] else None,
        "is_deleted": item["is_deleted"] if "is_deleted" in item and item["is_deleted"] else None,
        "uid": item["uid"] if "uid" in item and item["uid"] else None,
        "author_id": item["author_id"] if "author_id" in item and item["author_id"] else None,
        "date_added": item["date_added"] if "date_added" in item and item["date_added"] else None,
        "date_modified": item["date_modified"] if "date_modified" in item and item["date_modified"] else None
    }


class MatchSchema(BaseModel):
    participant_uids: List[str]
    competition_step_uid: str
    start_date: datetime
    duration: int
    is_deleted: bool
    uid: str
    author_id: str
    date_added: datetime
    date_modified: datetime