# from bson.raw_bson import RawBSONDocument
from pydantic import BaseModel
from typing import Optional

class TokenPayload(BaseModel):
    sub: Optional[str] = None


def userEntity(item: any) -> dict:
    return {
        # 'id': str(item["_id"]),
        'firstname': item["firstname"],
        'lastname': item["lastname"],
        'email': item["email"],
        'phonenumber': item["phonenumber"],
        'public_id': str(item["public_id"]) if "public_id" in item else "",
        'role_id': item["role_id"] if "role_id" in item else 0,
        'is_deleted': item["is_deleted"] if "is_deleted" in item else None
    }

def defaultEntity(item) -> dict:
    return {
        "id": item["id"] if 'id' in item and item["id"] else None,
        "title_fr": item["title_fr"] if 'title_fr' in item and item["title_fr"] else None,
        "title_en": item["title_en"] if 'title_en' in item and item["title_en"] else None,
    }