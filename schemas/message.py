from pydantic import BaseModel


class Message(BaseModel):
    detail: str


class Token(BaseModel):
    token: str

class AccesKey(BaseModel):
    access_key: str