
from typing import Any


def NotificationEntity(item: any)-> Any:
    return {
        "id": item["id"] if "id" in item else None,
        "notif_type": item["notif_type"] if "notif_type" in item else None,
        "payload_json": item["payload_json"] if "payload_json" in item else None,
        "read": item["read"] if "read" in item else None,
        "date_added": item["date_added"] if "date_added" in item else None,
    }