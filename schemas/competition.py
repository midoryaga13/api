from datetime import datetime
from typing import List, Optional
# from bson.raw_bson import RawBSONDocument
from pydantic import BaseModel

def competitionEntity(item: any)-> dict:
    return {
        "name": item["name"],
        "start_date": item["start_date"],
        "end_date": item["end_date"],
        "detail_about": item["detail_about"] if "detail_about" in item else None,
        "sport_id": item["sport_id"] if "sport_id" in item else None,
        "is_deleted": item["is_deleted"] if "is_deleted" in item else None,
        "date_added": item["date_added"] if "date_added" in item else None,
        "uid": str(item["uid"]) if "uid" in item else None,
        "max_number_of_participants": item["max_number_of_participants"] if "max_number_of_participants" in item else None,
        "registration_deadline": item["registration_deadline"] if "registration_deadline" in item else None,
        "competition_format_id": item["competition_format"] if "competition_format" in item else None,
        "registration_number": item["registration_number"] if "registration_number" in item else None,
        "rules": item["rules"] if "rules" in item else None,
        "location": item["location"] if "location" in item else None
    }

def CategoryEntity(item: any) -> dict:
    return {
        "type": item["type"] if "type" in item and item["type"] else "",
        "name": item["name"] if "name" in item and item["name"] else "",
        "rules": item["rules"] if "rules" in item and item["rules"] else "",
        "start_date": item["start_date"] if "start_date" in item and item["start_date"] else "",
        "end_date": item["end_date"] if "end_date" in item and item["end_date"] else "",
        "competition_format": item["competition_format"] if "competition_format" in item and item["competition_format"] else 0,
        "max_number_of_participants": item["max_number_of_participants"] if "max_number_of_participants" in item and item["max_number_of_participants"] else 0,
        "uid": item["uid"] ,
        "is_deleted": item["is_deleted"] if "is_deleted" in item else None,
        "date_added": item["date_added"] if "date_added" in item and item["date_added"] else "",
        "date_modified": item["date_modified"] if "date_modified" in item and item["date_modified"] else ""
    }

def StepEntity(item: any) -> dict:
    return {
        "uid": item["uid"],
        "is_deleted": item["is_deleted"],
        "start_date": item["start_date"] if "start_date" in item and item["start_date"] else None,
        "end_date": item["end_date"] if "end_date" in item and item["end_date"] else None,
        "competition_format_id": item["competition_format_id"] if "competition_format_id" in item and item["competition_format_id"] else None,
        "max_number_of_participants": item["max_number_of_participants"] if "max_number_of_participants" in item and item["max_number_of_participants"] else None,
        "status": item["status"] if "status" in item and item["status"] else None,
        "date_added": item["date_added"] if "date_added" in item and item["date_added"] else None,
        "date_modified": item["date_modified"] if "start_date" in item and item["start_date"] else None,
        "category_uid": item["category_uid"] if "category_uid" in item and item["category_uid"] else None,
        "id": item["id"] if "id" in item else None
    }

class StepSchema(BaseModel):
    uid: Optional[str] = None
    is_deleted: Optional[bool] = None 
    start_date: Optional[datetime] = None 
    end_date: Optional[datetime] = None 
    competition_format_id: Optional[int] = None 
    max_number_of_participants: Optional[int] = None 
    status: Optional[str] = None 
    date_added: Optional[datetime] = None 
    date_modified: Optional[datetime] = None 
    category_uid: Optional[str] = None 
    id: Optional[int] = None 

class CategorySchema(BaseModel):
    type: Optional[str]
    name: Optional[str]
    rules: Optional[str]
    start_date: Optional[datetime]
    end_date: Optional[datetime]
    competition_format: Optional[int]
    max_number_of_participants: Optional[int]
    uid: Optional[str]
    is_deleted: Optional[bool]
    date_added: Optional[datetime]
    date_modified: Optional[datetime]
class CompetitionSchema(BaseModel):
    name: Optional[str]
    start_date: Optional[datetime]
    end_date: Optional[datetime]
    event_type: Optional[str]
    sport_id: Optional[int]
    is_deleted: Optional[str]
    date_added: Optional[datetime]
    uid: Optional[str]
    max_number_of_participants: Optional[int]
    registration_deadline: Optional[datetime]
    competition_format_id: Optional[datetime]
    registration_number: Optional[int]

class CompetitionDetailSchema(BaseModel):
    competition: Optional[CompetitionSchema]
    categories: Optional[List[CategorySchema]]
    steps: Optional[List[StepSchema]]