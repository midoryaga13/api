import time

from celery import Celery

from core.config import Config
from utils.helper import my_monitor


celery = Celery(__name__)
celery.conf.timezone = 'Europe/Paris'
celery.conf.broker_url = Config.CELERY_BROKER_URL
celery.conf.result_backend = Config.CELERY_RESULT_BACKEND



@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    pass
    # sender.add_periodic_task(60*5, mail.automatic_reminder_sent_by_email_to_the_guest_before_30_min.s(
    # ), name='automatic reminder check 30 mins')

    # sender.add_periodic_task(60*5, mail.automatic_reminder_sent_by_email_to_the_guest_before_10_min.s(
    # ), name='automatic reminder check 10 mins')

    # sender.add_periodic_task(60*5, mail.automatic_close_session_after_10_min.s(
    # ), name='automatic close session after 10 min')

    # sender.add_periodic_task(
    #     crontab(hour=13, minute=00), 
    #     mail.automatic_reminder_sent_by_email_to_the_user_unactived_account.s(
    # ), name='automatic reminder sent by email to the user unactived account')

    # sender.add_periodic_task(60*1, mail.automatic_send_alert_session_remaining_10_min.s(
    # ), name='automatic reminder check 10 mins or 24 hours')

    # sender.add_periodic_task(60*1, mail.automatic_send_alert_session_remaining_1_min.s(
    # ), name='automatic reminder check 1 mins')

    # sender.add_periodic_task(60*1, mail.automatic_close_session_after_1_min.s(
    # ), name='automatic reminder check 1 mins after')

    # Executes every Monday morning at 7:30 a.m.
    # sender.add_periodic_task(
    #     crontab(hour=7, minute=30, day_of_week=1),
    #     test.s('Happy Mondays!'),
    # )


@celery.task(name="create_task")
def create_task(task_type):
    time.sleep(int(task_type) * 10)
    return True