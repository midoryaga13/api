
from typing import Any, List
from fastapi import APIRouter, Body, Depends
from core.i18n import t
from crud.match_crud import CRUDMatch
from models.match import ActivateMatch, CreateMatch, UpdateMatch
from models.user import User
from core import dependencies
from schemas.match import MatchSchema

from schemas.message import Message


router = APIRouter(
    prefix="/match",
    tags=["match"]
)

@router.post("/", response_model=Message)
async def create_match(
    match: CreateMatch,
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator", "auditor"]))
) -> Any:
    CRUDMatch.create_match(match=match, user_id= current_user["public_id"])
    return {"detail": t("match-created-successful")}

@router.put("/", response_model=Message)
async def update_a_match(
    match: UpdateMatch,
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator", "auditor"]))
) -> Any:
    CRUDMatch.update_a_match(match=match, user_id= current_user["public_id"])
    return {"detail": t("match-updated-successful")}
    
@router.delete("/", response_model=Message)
async def cancel_a_match(
    match_uid: str= Body(...),
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator", "auditor"]))
) -> Any:
    CRUDMatch.cancel_a_match(match_uid= match_uid)
    return {"detail": t("match-cancled-successful")}

@router.get("/")
async def get_matchs(
    step_uid: str,
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator", "auditor"]))
) -> Any:
    res = CRUDMatch.get_matchs(step_uid= step_uid)
    return res

# @router.get("/")
# async def get_match_trees(
#     step_uid: str,
#     current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator", "auditor"]))
# ) -> Any:
#     res = CRUDMatch.get_match_trees(step_uid= step_uid)
#     return res

@router.post("/step/")
async def auto_generate_matchs(
    match: CreateMatch,
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator", "auditor"]))
) -> Any:
    res = CRUDMatch.auto_generate_matchs(user_id=current_user["public_id"], match= match)
    return res

@router.put("/activate", response_model=Message)
async def activate_match(
    obj_in: ActivateMatch,
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator", "auditor"]))
) -> Any:
    CRUDMatch.activate_match(obj_in= obj_in)
    return {"detail": t("match-activated-successful")}
