
from typing import Any, List
from fastapi import APIRouter, Body, Depends
from core.i18n import t
from crud.competition_crud import CRUDCompetition
from core import dependencies

from models.competition import  CompetitionRegistration, CreateCompetition, CreateCompetitionStep, UpdateCompetition
from models.user import  User
from schemas.competition import CompetitionDetailSchema
from schemas.message import Message


router = APIRouter(
    prefix="/competition",
    tags=["competition"]
)

@router.get("/")
async def get_competitions( 
    # current_user : User = Depends(dependencies.TokenRequired())
    ):
    res = CRUDCompetition.get_list_competitions()
    return {"data": res}

@router.delete("/{uid}", response_model= Message)
async def delete_a_competition(
    uid: str, 
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator"]))
    ):
    CRUDCompetition.delete_a_competition(uid=uid)
    return {"detail": t("competition-delete-successful")}

@router.put("/", response_model= Message)
async def update_a_competition(
    data: UpdateCompetition = Body(...), 
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator"]))
    ):
    CRUDCompetition.update_a_competition(competition=data)
    return {"detail": t("competition-update-successful")}

@router.post("/", response_model= Message)
async def create_a_competition(
    competition: CreateCompetition = Body(...),
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator"]))
    ):
    CRUDCompetition.create_a_competition(competition=competition)
    return {"detail": t("competition-create-successful")}

@router.post("/step", response_model= Message)
async def add_a_step_for_a_competition(
    data: CreateCompetitionStep = Body(...),
    current_user: User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator"]))
    ):
    CRUDCompetition.add_a_step_for_a_competition(step=data)
    return {"detail": t("step-competition-added-successful")}

@router.post("/candidate", response_model=Message)
async def add_candidate(
    obj_in: CompetitionRegistration, 
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator", "user"]))
) -> Any:
    CRUDCompetition.add_candidate(user_id= current_user["public_id"], obj_in= obj_in)
    return {"detail": t("candidature-added-successful")}

@router.get("/candidate")
async def list_competition_candidates(
    competition_uid: str, 
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator", "user"]))
) -> Any:
    res = []
    res = CRUDCompetition.list_competition_candidates(competition_uid= competition_uid)
    return res
    
@router.put("/candidate", response_model= Message)
async def cancel_a_candidature(
    candidature_uid: str, 
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator", "user"]))
) -> Any:
    CRUDCompetition.cancel_a_candidature(candidature_uid= candidature_uid)
    return {"detail": t("candidature-canceled-successful")}@router.put("/candidate", response_model= Message)

@router.put("/step/close", response_model= Message)
async def cancel_a_candidature(
    step_uid: str, 
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator"]))
) -> Any:
    CRUDCompetition.close_competition_step(step_uid== step_uid)
    return {"detail": t("step-closed-successful")}

@router.put("/step/close", response_model= Message)
async def cancel_a_candidature(
    step_uid: str, 
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator"]))
) -> Any:
    CRUDCompetition.cancel_competition_step(step_uid== step_uid)
    return {"detail": t("step-canceled-successful")}