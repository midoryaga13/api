
from core import dependencies
from core.i18n import t
from models.user import RegisterSimpleUser, RegisterUser, UpdateUser, User
from schemas.message import Message
from schemas.user import *
from fastapi import APIRouter, Depends, File, Form, HTTPException, UploadFile
from crud.base import CRUDBase
from typing import Any
from config.database import fs, db
from utils.helper import upload_file

router = APIRouter(prefix="/users", tags=["users"])

@router.get('/')
async def get_users(
    current_user : User = Depends(dependencies.TokenRequired())
    ):
    return CRUDBase.get_multi(user= current_user)

@router.post('/', response_model=Message)
async def register_user(
    user: RegisterUser,
    current_user : User = Depends(dependencies.TokenRequired(roles=["administrator", "sub-administrator"]))
    ):
    CRUDBase.create(user= user)
    return {"detail": t("user-added-successful")}

@router.post('/s', response_model=Message)
async def register_s(
    user: RegisterSimpleUser,
    ):
    CRUDBase.create_simple_user(user= user)
    return {"detail": t("account-created-successful")}

@router.get('/one')
async def get_user(
    email: str, 
    current_user : User = Depends(dependencies.TokenRequired())):
    return CRUDBase.get_by_mail(email= email)


@router.put("/{user_id}")
def update_user(
    *,
    user_id: str, 
    user: UpdateUser, 
    current_user : User = Depends(dependencies.TokenRequired())
) -> Any:

    return CRUDBase.update(user_id= user_id, obj_in= user)

@router.get("/default_data")
def get_default_data() -> Any:
    print("Bonjour")
    return CRUDBase.get_default_data()

@router.post("/files/")
async def create_file(
    file: UploadFile = File(...),
    user_id: str = Form(...)
):
    if not db.user.find_one({"$and": [{"public_id": user_id}, {"$or": [{"is_deleted": False}, {"is_deleted": None}]}]}):
        raise HTTPException(
                status_code=404,
                detail= t("user-not-exist")
            )
    avatar = upload_file(file, user_id)
    print(avatar)
    db.user.update_one(
        {"$and": [{"public_id": user_id}, {"$or": [{"is_deleted": False}, {"is_deleted": None}]}]},
        update={"$set": {"avatar": avatar}},
        upsert=False
    )
    return avatar