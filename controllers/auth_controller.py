from datetime import timedelta
from typing import Any
from fastapi import APIRouter, Body, HTTPException
from core.config import Config
from crud.base import CRUDBase
from crud.user_crud import CRUDUser
from config.database import db
import core.security as security

from core.i18n import get_language, t
from models.user import UserLogin
from schemas.message import Message
from schemas.user import userEntity
from bson import ObjectId

router = APIRouter(
    prefix="/auth",
    tags=["auth"]
)

@router.post('/login')
async def user_login(*,
data: UserLogin 
):
    """
    Login user
    """
    print("data to submit {}".format(data))
    user = CRUDUser.authenticate(email=data.email, password=data.password)
    if not user:
            raise HTTPException(
                status_code=404,
                detail= t("user-not-exist")
            )
    access_token_expires = timedelta(
        minutes=Config.ACCESS_TOKEN_EXPIRE_MINUTES)
    return {
        "message": t("connect-success"),
        "token": {
            "access_token": security.create_access_token(
                user["public_id"], expires_delta=access_token_expires
            ),
            "token_type": "bearer",
        },
        "user": userEntity(user),
    }

@router.post("/password-recovery/{email}", response_model= Message)
async def recovery_password(email: str, ) -> Any:
    """
    Password Recovery
    """
    user = CRUDBase.get_by_mail(email=email)
    
    # prefered language
    prefered_language = get_language()
    password_reset_token = security.generate_password_reset_token(email=email)
    # mail.send_reset_password_email(
    #     email_to=user.email, email=email, token=password_reset_token.decode("utf-8"), username=user.fullname, prefered_language=prefered_language
    # )
    return {"message": password_reset_token}

@router.put('/reset-password', response_model=Message)
def reset_password(
    token: str = Body(...),
    new_password: str = Body(...),
) -> Any:
    """
    Reset password
    """
    email = security.verify_password_reset_token(token)
    if not email:
        raise HTTPException(status_code=400, detail= t("token-invalid"))
    print(email)
    user = CRUDBase.get_by_mail(email=email)
    if not user:
        raise HTTPException(
            status_code=404,
            detail=t("user-email-not-exist"),
        )
    # elif not crud.user.is_active(user):
    #     raise HTTPException(
    #         status_code=400, detail=__("user-inactive-account"))
    hashed_password = security.get_password_hash(new_password)
    db.user.find_one_and_update({"_id": ObjectId(user["id"])},
                { "$set": {"password": hashed_password}})
    return {"message": t("password-update-success")}
