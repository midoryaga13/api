from fastapi import APIRouter, Body
from core.i18n import t
from crud.migratrion_crud import CRUDMigration

from schemas.message import AccesKey


router = APIRouter(
    prefix="/migration",
    tags=["migration"]
)

@router.post("/roles")
async def merge_roles(data : AccesKey = Body(...), ):
    CRUDMigration.default_user_role(access_key = data.access_key)
    return {"detail": t("default-migration-role-success")}

@router.post("/sports")
async def merge_sports(
    *,
    data : AccesKey = Body(...)
    ):
    CRUDMigration.default_sport(access_key = data.access_key)
    return {"detail": t("default-migration-sport-success")}

@router.post("/competition-formats")
async def merge_competition_formats(
    *,
    data : AccesKey = Body(...)
    ):
    CRUDMigration.default_competition_format(access_key = data.access_key)
    return {"detail": t("default-migration-competition-format-success")}

@router.post("/genders")
async def merge_gensders(
    *,
    data : AccesKey = Body(...)
    ):
    CRUDMigration.default_gender(access_key = data.access_key)
    return {"detail": t("default-migration-gender-success")}

@router.post("/score-criteria")
async def merge_score_criteria(
    *,
    data : AccesKey = Body(...)
    ):
    CRUDMigration.default_score_criteria(access_key = data.access_key)
    return {"detail": t("default-migration-score-criteria-success")}