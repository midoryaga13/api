from .user_controller import router as user
from .auth_controller import router as auth
from .competition_controller import router as competition
from .migration_controller import router as migration
from .match_controller import router as match
from .voting_controller import router as voting
from fastapi import APIRouter

main_router = APIRouter()
main_router.include_router(user)
main_router.include_router(auth)
main_router.include_router(competition)
main_router.include_router(migration)
main_router.include_router(match)
main_router.include_router(voting)