
from typing import Any
from fastapi import APIRouter, Depends
from core.i18n import t
from models.user import User
from crud.voting_crud import CRUDVote
from models.voting import ScoringModel
from schemas.message import Message
from core import dependencies


router = APIRouter(
    prefix="/voting",
    tags=["voting"]
)

@router.post("/", response_model=Message)
async def register_a_score(
    data: ScoringModel,
    current_user : User = Depends(dependencies.TokenRequired(
        roles=["administrator", "sub-administrator", "jury"])
    )
) -> Any:
    CRUDVote.register_a_score(data=data)
    return {"detail": t("vote-registered-successful")}
