from datetime import datetime
import re

from fastapi import HTTPException
import phonenumbers
from config.database import fs, db

from core.i18n import t

def validate_email(email):
    EMAIL_REGEX = re.compile(r"[^@]+@[^@]+\.[^@]+")
    return EMAIL_REGEX.match(email)

def is_valid_phonenumber(number):
    try:
        y = phonenumbers.parse(f"{number}", None)
        if not phonenumbers.is_valid_number(y):
            return False
        return phonenumbers.format_number(y, phonenumbers.PhoneNumberFormat.E164)
    except Exception as e:
        return False
        
def validate_date(date_text=None, time_text=None):
    try:
        if date_text:
            validdate = datetime.strptime(date_text, '%Y-%m-%d %H:%M:%S')
            return validdate
        if time_text:
            validtime = datetime.strptime(time_text, '%H:%M:%S')
            return validtime
    except ValueError:
        if date_text:
            raise HTTPException(
                    status_code=422,
                    detail= f'{t("invalid-date-format")}: { date_text }'
                )
        if time_text:
            raise HTTPException(
                    status_code=422,
                    detail= f'{t("invalid-time-format")}: { time_text }'
                )

def my_monitor(app):
    state = app.events.State()

    def announce_failed_tasks(event):
        state.event(event)
        # task name is sent only with -received event, and state
        # will keep track of this for us.
        task = state.tasks.get(event['uuid'])

        print('TASK FAILED: %s[%s] %s' % (
            task.name, task.uuid, task.info(),))

    with app.connection() as connection:
        recv = app.events.Receiver(connection, handlers={
                'task-failed': announce_failed_tasks,
                '*': state.event,
        })
        recv.capture(limit=None, timeout=None, wakeup=True)

def upload_file(file: bytes, user_id: str)->dict:
    fs.put(file.file, filename= file.filename)
    data = db.fs.files.find_one({"filename": file.filename})
    outputdata = fs.get(data["_id"]).read()
    location = "static/images/"+file.filename
    out = open(location, 'wb')
    out.write(outputdata)
    out.close()
    return {
        "url": location,
        "uid": str(data["_id"]),
        "user_id": user_id
    }