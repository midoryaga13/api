<?php
session_start();
if(!isset($_GET['page'])){
    header('Location: login.php');
}
else{
    $path = "View/".$_GET['page'];
    $_SESSION['page']=$path;

    if(isset($_GET['modify'])){
        $_SESSION['modify'] = $_GET['modify'];
    }
    
    if(isset($_GET['modifyDepart'])){
        $_SESSION['modifyDepart'] = $_GET['modifyDepart'];
    }

    if(isset($_GET['modifyNiv'])){
      
        $_SESSION['modifyNiv'] = $_GET['modifyNiv'];
    }

    if(isset($_GET['modifyFil'])){
        $_SESSION['modifyFil'] = $_GET['modifyFil'];
    }

    if(isset($_GET['modifyOpt'])){
        $_SESSION['modifyOpt'] = $_GET['modifyOpt'];
    }

    if(isset($_GET['modifyUE'])){
        $_SESSION['modifyUE'] = $_GET['modifyUE'];
    }

    if(isset($_GET['modifyMat'])){
        $_SESSION['modifyMat'] = $_GET['modifyMat'];
    }

    if(isset($_GET['modifyCah'])){
        $_SESSION['modifyCah'] = $_GET['modifyCah'];
    }
    
    if(isset($_GET['modifyEns'])){
        $_SESSION['modifyEns'] = $_GET['modifyEns'];
    }

    if(isset($_GET['modifyDel'])){
        $_SESSION['modifyDel'] = $_GET['modifyDel'];
    }

     if(isset($_GET['details'])){
        $_SESSION['details'] = $_GET['details'];
    }

    if ($_GET['page']==='logout') {
        header('Location:../cahiernumerique/Config/logout.php');
    }
    else {
        if(isset($_GET['user'])){
            $_SESSION['auth']=$_GET['user'];
            $_SESSION['role']=$_GET['role'];
            // $_SESSION['role']
        }
        
        // echo $_SESSION['auth'];
        header('Location:../cahiernumerique/'.$path);
    }
    
}
