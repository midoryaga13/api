from logging import debug
import secrets
from fastapi import Depends, FastAPI, HTTPException, status
from starlette.middleware.base import BaseHTTPMiddleware
from fastapi.middleware.cors import CORSMiddleware
from core.config import Config
from core.i18n import add_process_language_header
from controllers.router import main_router
from fastapi.staticfiles import StaticFiles
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.openapi.docs import get_swagger_ui_html, get_redoc_html
from fastapi.openapi.utils import get_openapi

from schemas.message import Message

description = '''
    This is ESM API. All request in regards to the application can be found here,
    in general most of the methods require you being as an active user to be able to access.
'''

security = HTTPBasic()

app = FastAPI(
    title=Config.PROJECT_NAME, openapi_url=f"{Config.API_V1_STR}/openapi.json", description = description, version = f"{Config.PROJECT_VERSION}"
)

app.mount("/static", StaticFiles(directory="static"), name="static")
app.include_router(main_router, prefix=Config.API_V1_STR)
app.add_middleware(BaseHTTPMiddleware, dispatch=add_process_language_header)
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=["*"]
)

def get_current_username(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = secrets.compare_digest(credentials.username, Config.ADMIN_KEY)
    correct_password = secrets.compare_digest(credentials.password, Config.ACCESS_ADMIN_KEY)
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials.username

@app.get("/docs", include_in_schema=False)
async def get_swagger_documentation(username: str = Depends(get_current_username)):
    return get_swagger_ui_html(openapi_url="/openapi.json", title="docs")

@app.get("/redoc", include_in_schema=False)
async def get_redoc_documentation(username: str = Depends(get_current_username)):
    return get_redoc_html(openapi_url="/openapi.json", title="docs")


@app.get("/openapi.json", include_in_schema=False)
async def openapi(username: str = Depends(get_current_username)):
    return get_openapi(title=app.title, version=app.version, routes=app.routes, description=app.description)

@app.get("/ping", response_model=Message, status_code=200)
async def test_ping():
    return {"message": "Pong"}
